/*

Separation of Concerns 

Paghihiwalay ng models, routes, controllers... 

Why?
	-better code readability
	-improved scalability
	-better code maintanability 

What? 
	-Models
	-Controllers
	-Routes 

Models 
	-contains what objects are needed in our API 
		ex. users, courses 
	-object schemas and relationships are defined here 
		-schemas are JSON objects that define structure and content of document (blueprint)

Controllers 
	-contains instructions on HOW your API will perform its intended tasks 
	-Mongoose model queries are used here, examples are:
		-Model.find()
		-Model.findByIdAndRemove()

Routes
	-defines When particular contollers will be used 
	-A specific controller action will be called when aspecific HTTP method is received on a specific API endpoint 

JS Modules
	-are self-contained units of functionality that can be shared and reused 

	-creating and exporting
	-then importing (using require directive) 

route endpoint 
app.use('/api/courses', courseRoutes)


*/ 

//Setup the Dependencies 
const express = require('express');
const mongoose = require('mongoose');

//This will allows us to use all the routes defined in the "taskRoute.js"
const taskRoute = require('./routes/taskRoute');


//Server Setup
const app = express();
const port = 3001;

//Middleware (api)
app.use(express.json());	//JSON file
app.use(express.urlencoded({extended: true}));	//Forms

//Database Connection
mongoose.connect("mongodb+srv://admin:admin123@course-booking.fyojt.mongodb.net/B157_to-do?retryWrites=true&w=majority",{
		useNewUrlParser: true,
		useUnifiedTopology: true
	});


let db = mongoose.connection;
db.on('error', console.error.bind(console,'Connection Error'));
db.once('open', () => console.log(`We're connected to the cloud database`));

//Add the task route 
//Allow all the task routes create in the "taskRoute.js"
app.use('/tasks', taskRoute);
//http:localhost:3001/tasks/




app.listen(port, () => console.log(`Server now running at localhost:${port}`));